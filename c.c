/*

gcc c.c -lqd -Wall -DUSE_QD_REAL
 export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib/
 ./a.out 





---------- git ------------------

cd existing_folder
git init
git remote add origin git@gitlab.com:adammajewski/InfoldingSiegelDisk_in_c_1over3_quaddouble.git
git add c.c
git commit -m "commit"
git push -u origin master

--------- description --------------
https://commons.wikimedia.org/wiki/File:InfoldingSiegelDisk1over3.gif
https://en.wikibooks.org/wiki/Fractals/Iterations_in_the_complex_plane/siegel#1.2F3




*/

#include <stdio.h>
#include <math.h>
#include <qd/c_qd.h> // 64 decimal digits 



// phi = (1+sqrt(5))/2 = golden ratio
// https://en.wikipedia.org/wiki/Golden_ratio
int GivePhi(double p[4]){
 

  double a[4];
  
  


  c_qd_copy_d(5.0, p); // p = 5.0
  c_qd_copy_d(1.0, a); // a = 1.0

  c_qd_sqrt(p, p); // p = sqrt(p) =sqrt(5)
  c_qd_add(p, a, p); // p= a+p = 1+sqrt(5)
  c_qd_selfmul_d(0.5, p); // p = p*0.5 = p/2 = phi
  
  return 0;

}



/*


 compute floating point number from continued fraction
 t(n) = [0;3,10^n,phi]

      = 1/ (3 + (1 /(p + (1/phi))))


*/



void GiveT(int n,  double t[4]){

double p[4], a[4], b[4], one[4],  three[4];

double phi[4];

  
 //
 c_qd_copy_d(1.0, one); // one = 1.0
 c_qd_copy_d(3.0, three); // one = 1.0
 c_qd_copy_d(10.0, a); // a = 10.0
 c_qd_npwr(a, n, p); // p = a^n
 GivePhi(phi); 
 c_qd_div(one,phi,b); // b= one/phi = 1/phi
  
 c_qd_add(p,b,t); // t= p+b = p+1/phi = (a^n) +1/phi
 
 c_qd_copy(one,b);
 c_qd_selfdiv(t,b); //b= 1/t
 
 c_qd_add(three,b, t); // t = 3+b   
 

 c_qd_copy(one,b);
 c_qd_selfdiv(t,one); //t= 1/b
 //c_qd_write(b); // print 

 c_qd_copy(one,t); 
 

}


void Rat2float(double numerator, double denominator, double result[4]){

 double n[4];
 double d[4];

 c_qd_copy_d(numerator, n); // one = 1.0
 c_qd_copy_d(denominator, d); // one = 1.0

 c_qd_div(n, d, result); // b= one/phi = 1/phi
 c_qd_write(result); // print 


}

/* 
  compute complex number c = point on the boundary of period 1 component of Mandelbrot set 
  
  double a = t *2*M_PI; // from turns to radians
  double cx, cy; 
  // c = cx+cy*i 
  cx = (cos(a))/2-(cos(2a))/4; 
  cy = (sin(a))/2-(sin(2a))/4;

  input t
  output cx,cy  where c = cx + cy*I

*/
void GiveC( double t[4], double cx[4], double cy[4]){
double  a[4];
double a2[4];
double  p[4];
// 
double   s[4];
double  s2[4];



 c_qd_pi(p);
 // printf("pi = "); c_qd_write(p); printf(" \n");  
 c_qd_selfmul_d(2.0,p); // p = 2 *pi
 c_qd_mul(t, p,  a);

 // printf("pi = "); c_qd_write(p); printf(" \n");  
        //  a =   t*p
 c_qd_mul_qd_d(a, 2.0, a2); // a2 = 2*t*p
 //
 c_qd_cos(a,s);   // s  = cos(a)
 c_qd_cos(a2,s2); // s2 = cos(a2)
 //
 c_qd_selfdiv_d(2.0,s);   // s  = s/2
 c_qd_selfdiv_d(4.0,s2);  // s2 = s/4 
 //
 c_qd_sub(s,s2,cx); // cx = s - s2 

 //

 c_qd_sin(a,s);   // s  = sin(a)
 c_qd_sin(a2,s2); // s2 = sin(a2)
 //
 c_qd_selfdiv_d(2.0,s);   // s  = s/2
 c_qd_selfdiv_d(4.0,s2);  // s2 = s/4 
 //
 c_qd_sub(s,s2,cy); // cy = s - s2 




}




int main(void) {

  // n -> t -> c 
  int n;
  double t[4];
  double cx[4];
  double cy[4];


  
  fpu_fix_start(NULL); // turns on the round-to-double bit in the FPU control word on Intel x86 Processors. 

 
  for (n=0; n<12; n++){
      // n -> t 
      GiveT(n,t);
      printf("t(%d) = ", n); c_qd_write(t); printf("\n");
      // t -> c
      GiveC(t, cx,cy);
      printf("c = "); c_qd_write(cx); printf(" ; "); c_qd_write(cy); printf(" \n"); 
      
      
     }
  Rat2float( (double) 1, (double) 3, t); 
  // t -> c
  GiveC(t, cx,cy); 
  printf("c = "); c_qd_write(cx); printf(" ; "); c_qd_write(cy); printf(" \n"); 
 


  
  //
  fpu_fix_end(NULL);

  return 0; 
}
